var form = document.getElementById("form");
var numberInput = document.getElementById("numberInput");
var addNumberButton = document.getElementById("addNumber");
var numbersDiv = document.getElementById("numbers");
var sumPositiveNumbersButton = document.getElementById("sumPositiveNumbers");
var resultDiv = document.getElementById("result");

var numbers = [];

addNumberButton.addEventListener("click", function () {
  var number = Number(numberInput.value);
  numbers.push(number);
  numberInput.value = "";
  numbersDiv.innerHTML += `${number}` + ", ";
});

sumPositiveNumbersButton.addEventListener("click", function () {
  var sum = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      sum += numbers[i];
    }
  }
  resultDiv.innerHTML = `Sum of positive numbers: ${sum}`;
});
